import React, { Component } from 'react';
import { ScrollView, Image, StyleSheet, Dimensions } from 'react-native';

export class EventosBanner extends Component {
  render() {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={true}> 
        { this.props.data &&
            this.props.data.map((e, i) => {
              console.log(e.img);
              return <Image key={i} source={{ url: e.img }} style={estilos.image} />
            })}
      </ScrollView>
    );
  }
}

const { width } = Dimensions.get('window');
const estilos = StyleSheet.create({
    view: {},
    image: { height: 300, width: width, borderColor: '#000', borderWidth: 1 }
});
