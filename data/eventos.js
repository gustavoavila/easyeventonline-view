export const eventos = [
    {
        id: 1,
        nome: 'Como estudar com qualidade',
        img: '../assets/evento01.jpg',
        destaque: true
    },
    {
        id: 2,
        nome: 'Aprenda React Native na prática',
        img: '../assets/evento02.jpg',
        destaque: true
    },
    {
        id: 3,
        nome: 'Aprenda Spring na prática',
        img: '../assets/evento03.jpg',
        destaque: true
    }
]
