import PrivateViews from './PrivateViews';
import GuestViews from './GuestViews';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

const AppStack = PrivateViews;
const AuthStack = GuestViews;

export default createAppContainer(createSwitchNavigator(
    {
        Auth: AuthStack,
        App: AppStack
    },
    {
        initialRouteName: 'Auth'
    }
));
