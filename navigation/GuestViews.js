import { createStackNavigator } from 'react-navigation';
import Login from '../views/Login';
import Forgot from '../views/Forgot';
import EmpresaRegister from '../views/EmpresaRegister';

export default createStackNavigator(
    {
        Login,
        Forgot,
        EmpresaRegister,
    },
    {
        defaultNavigationOptions: {
            header: null
        }
    }
)