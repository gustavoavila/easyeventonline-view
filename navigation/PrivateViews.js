import React from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import Dashboard from '../views/Dashboard';
import Profile from '../views/Profile';
import EventoCadastro from '../views/EventoCadastro';
import { Ionicons } from '@expo/vector-icons'; // 6.2.2

export default createBottomTabNavigator(
	{
		Dashboard: {
			screen: Dashboard,
			navigationOptions: ({ navigation }) => ({
				tabBarIcon: ({ tintColor }) => {
					let IconComponent = Ionicons;
					let iconName;
					iconName = `ios-home`;
					return <IconComponent name={iconName} size={25} color={tintColor} />
				}
			}),
		},
		NovoEvento: {
			screen: EventoCadastro,
			navigationOptions: ({ navigation }) => ({
				title: 'Novo evento',
				tabBarIcon: ({ tintColor }) => {
					let IconComponent = Ionicons;
					let iconName;
					iconName = `ios-calendar`;
					return <IconComponent name={iconName} size={25} color={tintColor} />
				}
			}),
		},
		Profile: {
			screen: Profile,
			navigationOptions: ({ navigation }) => ({
				tabBarIcon: ({ tintColor }) => {
					let IconComponent = Ionicons;
					let iconName;
					iconName = `ios-person`;
					return <IconComponent name={iconName} size={25} color={tintColor} />
				}
			}),
		},
		
	},
	{
		defaultNavigationOptions: ({ navigation }) => ({
			tabBarOptions: {
				activeTintColor: '#6200ee',
				inactiveTintColor: 'gray'
			}
		})
	}
);
