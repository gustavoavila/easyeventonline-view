import React from 'react';
import { StyleSheet, View, FlatList, Dimensions, Image } from 'react-native';
import { Button, Text, Card, Title, Paragraph, Searchbar } from 'react-native-paper';
import axios from 'axios';
import { withNavigation } from "react-navigation";

const { width } = Dimensions.get('screen');

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eventos: [],
      message: null
    };
    
  }

  componentWillMount() {
    this.getEventos();
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.getEventos();
    })
  }

  getEventos() {
    this.setState({message: 'Carregando...'});
    axios.get('https://easyeventonline.herokuapp.com/api/eventos')
    .then(response => response.data)
    .then(json => {
      if (json._embedded && json._embedded.eventos) {
        this.setState({ eventos: json._embedded.eventos, message: null })
      } else {
        this.setState({ eventos: [], message: 'Nenhum evento cadastrado!'})
      }
       
    })
    .catch(error => console.log(error));
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  buscarEventoPorNome(nome) {
    axios.get(`https://easyeventonline.herokuapp.com/api/eventos?nome=${nome}`)
      .then(response => response.data)
      .then(json => this.setState({ eventos: json._embedded.eventos }))
      .catch(error => console.log(error));
  }

  excluirEvento(uri) {
    const id = uri.slice(46);
    axios.delete(`https://easyeventonline.herokuapp.com/api/eventos/${id}`)
      .then(() => {
        this.componentWillMount();
      })
      .catch(error => console.log(error));
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.Dashboard}>
      
        <View style={styles.Body}>
          <Searchbar
            style={{width: width}}
            placeholder='Buscar'
            onChangeText={query => {
              this.buscarEventoPorNome(query);
            }}
            // value={firstQuery}
          />

          {
            !this.state.message ?
              <FlatList
              keyExtractor={item => item.nome}
              data={this.state.eventos}
              renderItem={({ item }) => (
                <Card style={styles.Card}>
                  <Card.Title
                    title={item.nome}
                    subtitle={item.descricao}
                    //left={props => <Avatar.Icon {...props} icon='folder' />}
                  />
                  <Card.Cover 
                    source={{ uri: `data:image/png;base64,${item.imagemDivulgacao}` }}
                    />
                  <Card.Actions>
                    <Button
                      onPress={() => {
                        this.excluirEvento(item._links.self.href);
                      }}
                    >Excluir</Button>
                  </Card.Actions>
                </Card>
              )}
            />
          :
          <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>{this.state.message}</Text>
          </View>
          }
          

        </View>

        {/* <View style={styles.Footer}>
          <Button
            style={styles.Button}
            mode='contained'
            onPress={() => navigation.navigate('NovoEvento')}
            title='Novo evento'
          >
            Novo evento
          </Button>
        </View> */}
      </View>
    );
  }
}

export default withNavigation(Dashboard);

const styles = StyleSheet.create({
  Dashboard: {
    flex: 1,
    marginTop: 25,
    alignItems: 'center',
    justifyContent: 'center'
  },
  Card: {
    width: Dimensions.get('screen').width
  },
  Header: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  Body: {
    flex: 3,
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  Footer: {
    flex: 1
  },
  Button: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 60,
    marginRight: 60
  }
});
