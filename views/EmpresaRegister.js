import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button, Title, TextInput, Text } from 'react-native-paper';

export default class EmpresaRegister extends Component {
  state = {
    nomeUsuario: '',
    senha: ''
  };

  onSave(data) {
    return fetch('https://easyeventonline.herokuapp.com/api/empresas',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
      }
    )
    .then(console.log(data))
    .then(() => this.props.navigation.navigate('Dashboard'))
    .catch(error => console.log(error));
  }
  
  render() {
    const { navigation } = this.props;
    return (
        <View style={ styles.Login }>
          <View style={ styles.Titulo }>
            <Title>Cadastro</Title>
          </View>

          <View style={ styles.Form }>
            
            <TextInput
              style={ styles.Input }
              label='Nome de usuario'
              value={this.state.nomeUsuario}
              onChangeText={nomeUsuario => this.setState({ nomeUsuario })}
            />

            <TextInput
              style={ styles.Input }
              label='Senha'
              value={this.state.senha}
              secureTextEntry={true}
              onChangeText={senha => this.setState({ senha })}
            />

            <Button
              style={ styles.ButtonLogin }
              mode="contained"
              // onPress={() => navigation.navigate('Dashboard')}>
              disabled={this.state.nomeUsuario === '' || this.state.senha === ''}
              onPress={() => this.onSave(this.state)}>
              Cadastrar
            </Button>

          </View>

          <View style={ styles.Footer }>

          </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  Login:{
      flex:1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch'
  },
  Form: {
    flex: 1,
    justifyContent: 'center'
  },
  Titulo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  Input: {
    backgroundColor: '#fff',
    marginLeft: 20,
    marginRight: 20
  },
  ButtonLogin: {
    marginTop: 20,
    marginBottom: 20,
    marginLeft: 60,
    marginRight: 60
  },
  ButtonRegister: {
    fontSize: 5,
  },
  Footer: {
    flex: 1
  },
  Texto: {
    textAlign: 'center'
  }
})
