import React from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Image,
  Picker
} from 'react-native';
import {
  Headline,
  Subheading,
  TextInput,
  IconButton,
  Button
} from 'react-native-paper';
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import {ImagePicker, Permissions, Constants} from 'expo';
import { TextInputMask } from 'react-native-masked-text'
import axios from 'axios';
import { NavigationEvents } from 'react-navigation';

const { width } = Dimensions.get('screen');

const mapearCampos = (campos) => (
{ ...campos,
  cep: campos.cep.replace('-', ''),
  // dataHoraInicio: campos.dataHoraInicio.replace('+00:00', ''),
  tiposEventosTela: null,
  // imagemDivulgacao: ''
}
);

export default class EventoCadastro extends React.Component {
  constructor() {
    super();
    this.state = {
      tiposEventosTela: [],
      localidade: '',
      dataHoraInicioTela: '',
      isDateTimePickerVisible: false,
      nome: '',
      descricao: '',
      dataHoraInicio: '',
      dataHoraTermino: '2019-06-18T08:00:00',
      valor: '',
      maximoDeParticipantes: '',
      minimoDeParticipantes: '1',
      imagemDivulgacao: null,
      logradouro: '',
      numero: '',
      bairro: '',
      complemento: '',
      cep: '',
      cidadeId: '3500',
      tipoMeioContato01Id: '1',
      contato01: '',
      tipoMeioContato02Id: '2',
      contato02: '',
      empresaOrganizadoraId: '1',
      tipoEventoId: ''
    };
  }

  componentDidMount() {
    this.getPermissionAsync();
    this.getTiposEventos();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Desculpe, nós precisamos de acesso a câmera para funcionar');
      }
    }
  }

  showDateTimePicker = () => {
    this.setState({ ...this.state, isDateTimePickerVisible: true});
  }

  hideDateTimePicker = () => {
    this.setState({ ...this.state, isDateTimePickerVisible: false });
  }

  handDatePicked = datetime => {
    // console.log("A date has been picked: ", datetime);
    this.setState({ ...this.state, 
      dataHoraInicioTela: moment(datetime).format('DD/MM/YYYY, H:mm'), 
      dataHoraInicio: moment(datetime).format('YYYY-MM-DDTHH:mm:ss') })
    this.hideDateTimePicker();
    // console.log(this.state.dataHoraInicio);
  }

  getTiposEventos() {
    axios.get(`https://easyeventonline.herokuapp.com/api/tiposeventos/todos`)
      .then(response => {
        this.setState({tiposEventosTela: response.data});
      })
      .catch(error => {
        console.log(error);
      })
  }

  getEndereco() {
    if (this.state.cep !== '') {
      axios.get(`https://viacep.com.br/ws/${this.state.cep}/json/`)
        .then(response => response.data)
        .then(responseJson => {
          this.setState( 
            {...this.state,
              cep: responseJson.cep,
              logradouro: responseJson.logradouro,
              bairro: responseJson.bairro,
              complemento: responseJson.complemento,
              numero: ''
              
            }
          );
          this.setState({localidade: `${responseJson.localidade} - ${responseJson.uf}`})
        })
        .catch(error => {
          console.log(error);
        })
    }
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      base64: true
    });

    if (!result.cancelled) {
      this.setState({ imagemDivulgacao: result.base64 });
    }
  };

  // updateEndereco = (texto, nomeCampo) => {
  //   this.setState({ ...this.state, endereco: { ...this.state.endereco, [nomeCampo]: texto } })
  // }

  // updateContato = (texto, nomeCampo) => {
  //   this.setState({ ...this.state, meioscontato: { ...this.state.meioscontato, [nomeCampo]: texto } })
  // }

  salvarEvento = () => {
    const { navigation } = this.props;
    axios({
      method: 'POST',
      url: `https://easyeventonline.herokuapp.com/api/eventos`,
      data: JSON.stringify(mapearCampos(this.state)),
      headers: {'Content-Type': 'application/json; charset=utf-8'}
    })
    .then(res => {
      // console.log('Evento salvo com sucesso!');
      this.limparForm
      navigation.navigate('Dashboard');
      // console.log(JSON.stringify(mapearCampos(this.state)));
    })
    .catch(error => {
      console.log(error);
      console.log(JSON.stringify(mapearCampos(this.state)));
    })
  }

  limparForm = () => {
    this.setState({
      tiposEventosTela: [],
      localidade: '',
      dataHoraInicioTela: '',
      isDateTimePickerVisible: false,
      nome: '',
      descricao: '',
      dataHoraInicio: '',
      dataHoraTermino: '2019-06-18T08:00:00',
      valor: '',
      maximoDeParticipantes: '',
      minimoDeParticipantes: '1',
      imagemDivulgacao: null,
      logradouro: '',
      numero: '',
      bairro: '',
      complemento: '',
      cep: '',
      cidadeId: '3500',
      tipoMeioContato01Id: '1',
      contato01: '',
      tipoMeioContato02Id: '2',
      contato02: '',
      empresaOrganizadoraId: '1',
      tipoEventoId: ''
    })
  }

  render() {
    const { navigation } = this.props;
    return (
      <View>
        
        <NavigationEvents
          onWillBlur={payload => this.limparForm()}
        />
        
        <ScrollView contentContainerStyle={styles.ContentContainer}>
          <View style={styles.Header}>
            <Headline>Novo evento</Headline>
          </View>

          <View style={styles.Body}>
            <Subheading style={styles.Subtitulo}>Dados gerais</Subheading>
            <TextInput
              style={styles.Input}
              label='Nome'
              value={this.state.nome}
              onChangeText={nome => this.setState({ nome })}
            />

            <TextInput
              style={styles.Input}
              label='Descrição'
              value={this.state.descricao}
              onChangeText={descricao => this.setState({ descricao })}
            />

            <View style={{ flexDirection: 'row' }}>
              <TextInput
                style={styles.InputDateTime}
                label='Data/hora de ínicio'
                disabled={true}
                value={this.state.dataHoraInicioTela}
                onChangeText={dataHoraInicio =>
                  this.setState({ dataHoraInicio })
                }
              />

              <IconButton
                icon='date-range'
                size={25}
                color='#6200ee'
                onPress={this.showDateTimePicker}
              />
            </View>

            <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this.handDatePicked}
              onCancel={this.hideDateTimePicker}
              mode='datetime'
              datePickerModeAndroid='spinner'
              timePickerModeAndroid='spinner'
            />

            <View style={{ flexDirection: 'row' }}>
              <TextInput
                style={{ ...styles.Input, width: width * 0.5 }}
                label='Máx de participantes'
                value={this.state.maximoDeParticipantes}
                onChangeText={maximoDeParticipantes =>
                  this.setState({ maximoDeParticipantes })
                }
                render={props =>
                  <TextInputMask
                    {...props}
                    type={'only-numbers'}
                  />
                }
                
              />
              <TextInput
                style={{ ...styles.Input, width: width * 0.3 }}
                label='Valor'
                value={this.state.valor}
                onChangeText={valor => this.setState({ valor })}
                
                // render={props =>
                //   <TextInputMask
                //     {...props}
                //     type={'money'}
                //     options={{
                //       precision: 2,
                //       separator: ',',
                //       delimiter: '.',
                //       unit: 'R$',
                //       suffixUnit: ''
                //     }}
                    
                //   />
                // }
              />
            </View>
            
            <Picker
              selectedValue={this.state.tipoEventoId}
              style={styles.Dropdown}
              placeholder="Tipo do evento"
              onValueChange={(itemValue, itemIndex) =>
                this.setState({tipoEventoId: itemValue})
              }>
                <Picker.Item label="Tipo de evento" value="" />
                {
                  this.state.tiposEventosTela &&
                  this.state.tiposEventosTela.map((tipoevento, index) => {
                    return <Picker.Item key={index} label={tipoevento.descricao} value={index} />      
                  })
                }
            </Picker>

            <Button
              icon="add-a-photo"
              style={styles.ButtonSalvar}
              mode='outlined'
              onPress={this._pickImage}
            >
              Imagem de capa
            </Button>
            
            {
              this.state.imagemDivulgacao && 
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <Image source={{ uri: `data:image/png;base64,${this.state.imagemDivulgacao}` }} 
                    style={{ width: 150, height: 113 }}/>
                </View>  
            }

            <Subheading style={styles.Subtitulo}>Endereço</Subheading>

            <View style={{ flexDirection: 'row' }}>
              <TextInput
                style={ {...styles.InputLeft, width: width * 0.4} }
                label='CEP'
                value={this.state.cep}
                onChangeText={cep =>
                  this.setState({cep: cep})
                }
                onBlur={() => this.getEndereco()}
                render={props =>
                  <TextInputMask
                  {...props}
                  type={'only-numbers'}
                  />
                }
              />

              <TextInput
                style={ {...styles.InputRight, width: width * 0.4} }
                label='Número'
                value={this.state.numero}
                onChangeText={numero =>
                  this.setState({numero: numero})
                }
                render={props =>
                  <TextInputMask
                    {...props}
                    type={'only-numbers'}
                  />
                }
              />
            </View>

            <TextInput
              style={styles.Input}
              label='Logradouro'
              value={this.state.logradouro}
              disabled={true}
              onChangeText={logradouro =>
                this.setState({logradouro: logradouro})
              }
              />
            
            <View style={{ flexDirection: 'row' }}>
              <TextInput
                style={ {...styles.InputLeft, width: width * 0.3} }
                label='Bairro'
                value={this.state.bairro}
                disabled={true}
                onChangeText={bairro =>
                  this.setState({bairro : bairro})
                }
              />

              <TextInput
                style={ {...styles.InputRight, width: width * 0.5} }
                label='Localidade'
                value={this.state.localidade}
                disabled={true}
                onChangeText={localidade =>
                  this.setState({ localidade: localidade })
                }
              />
            </View>

            <Subheading style={styles.Subtitulo}>Contatos</Subheading>

            <TextInput
              style={styles.Input}
              label='Telefone'
              value={this.state.contato01}
              onChangeText={contato01 =>
                this.setState({contato01})
              }
              
              render={props =>
                <TextInputMask
                  {...props}
                  type={'custom'}
                  options={{
                    mask: "(99)99999-9999"
                  }}
                  
                />
              }
            />

            <TextInput
              style={styles.Input}
              label='E-mail'
              value={this.state.contato02}
              onChangeText={contato02 =>
                this.setState({contato02})
              }
            />
          </View>

          <View style={styles.Footer}>
            <Button
              style={styles.ButtonSalvar}
              mode='contained'
              onPress={() => {
                this.salvarEvento();
                // navigation.navigate('Dashboard');
              }}
                >
              Salvar evento
            </Button>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  ContentContainer: {
    paddingVertical: 25
  },
  Titulo: {},
  Subtitulo: {
    marginTop: 40,
    marginBottom: 10,
    marginLeft: 20
  },
  EventoCadastro: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch'
  },
  Body: {
    flex: 1,
    justifyContent: 'center'
  },
  Header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  Input: {
    backgroundColor: '#fff',
    marginLeft: 20,
    marginRight: 20
  },
  InputLeft: {
    backgroundColor: '#fff',
    marginLeft: 20,
  },
  InputRight: {
    backgroundColor: '#fff',
    marginLeft: 20,
  },
  InputDateTime: {
    backgroundColor: '#fff',
    marginLeft: 20,
    width: width * 0.8
  },
  ButtonSalvar: {
    fontSize: 5,
    margin: 20
  },
  Button: {
    width: width * 0.3,
    marginBottom: 20
  },
  Footer: {
    marginTop: 100
  },
  Texto: {
    textAlign: 'center'
  },
  Divisor: {
    margin: 30
  },
  Dropdown: {
    height: 50,
    marginLeft: 20,
    marginRight: 20,
    alignItems: 'stretch'
  }
});
