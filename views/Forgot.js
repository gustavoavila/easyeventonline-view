import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import DrawerButton from '../components/DrawerButton'

export default class Forgot extends Component {
  render() {
    return (
      <View style={styles.Forgot}>
      <DrawerButton  navigation={this.props.navigation}/>
        <Text> Esqueceu sua senha? </Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    Forgot:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})