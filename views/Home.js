import React from 'react';
import { View, Button } from 'react-native';
import { EventosBanner } from '../components';
import { eventos } from '../data/eventos'

export class Home extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={{ flex: 1, justifyContent: 'center'}}>
        <Button title='Cadastrar novo evento' onPress={() => navigation.navigate('EventoCadastro')} />
        <EventosBanner data={ eventos } />
      </View>
    );
  }
}
