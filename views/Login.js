import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Button, Title, TextInput, Text } from 'react-native-paper';

const width = Dimensions.get('screen').width;

export default class Login extends Component {
  state = {
    usuario: '',
    senha: ''
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.Login}>
        <Title>EasyEvent</Title>

        <View style={styles.Form}>
          <TextInput
            style={styles.Input}
            label='Usuario'
            value={this.state.usuario}
            onChangeText={usuario => this.setState({ usuario })}
          />

          <TextInput
            style={styles.Input}
            label='Senha'
            value={this.state.senha}
            secureTextEntry={true}
            onChangeText={senha => this.setState({ senha })}
          />

          <View style={styles.Footer}>
            <Button
              style={styles.Button}
              mode='contained'
              onPress={() => navigation.navigate('Dashboard')}
            >
              Entrar
            </Button>

            <Text style={styles.Texto}>Não possui conta?</Text>
            <Button
              mode='text'
              onPress={() => navigation.navigate('EmpresaRegister')}
            >
              Criar uma conta
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Login: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  Form: {
    width: width * 0.8
  },
  Input: {
    backgroundColor: '#fff'
  },
  Button: {
    width: width * 0.3,
    marginBottom: 20
  },
  Footer: {
    marginTop: 20,
    alignItems: 'center'
  }
});
