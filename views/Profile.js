import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import DrawerButton from '../components/DrawerButton';
import { Button, Text } from 'react-native-paper';

export default class Profile extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.Perfil}>
        {/* <DrawerButton navigation={this.props.navigation} /> */}
        <Text> Perfil </Text>
        <Button
          mode='text'
          onPress={() => navigation.navigate('Login')}
        >
          Sair
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Perfil: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
