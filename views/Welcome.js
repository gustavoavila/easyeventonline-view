import React from 'react';
import { View } from 'react-native';
import { Button, Title, Text } from 'react-native-paper';

export class Welcome extends React.Component {
  
  render() {
    const { navigation } = this.props;
    return (
        <View style={{ flex: 1, justifyContent: 'center'}}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Title>EasyEvent</Title>
          </View>

          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text>Seu evento à um click</Text>
          </View>
          
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            
            <Button 
              style={{ marginTop: 10, marginBottom: 10 }}
              mode="contained"
              onPress={() => navigation.navigate('Login')}>
              Entrar
            </Button>

            <Button
              style={{ marginTop: 10, marginBottom: 10 }} 
              mode="outlined"
              onPress={() => navigation.navigate('EmpresaRegister')}>
              Criar sua conta
            </Button>
            
          </View>

        </View>
    );
  }
}
